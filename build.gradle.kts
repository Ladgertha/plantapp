buildscript {

    repositories {
        jcenter()
        google()
    }

    dependencies {
        classpath(ClasspathDependency.GRADLE)
        classpath(ClasspathDependency.KOTLIN)
    }
}

allprojects {
    repositories {
        jcenter()
        google()
    }
}
