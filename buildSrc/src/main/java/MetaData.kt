object MetaData {
    const val buildToolsVersion = "29.0.2"
    const val compileSdkVersion = 29
    const val targetSdkVersion = 29
    const val minSdkVersion = 19

    const val versionCode = 1
    const val versionName = "1.0.0"

    const val applicationId = "ru.ladgertha.plantapp"
    const val testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
}