object Version {
    const val appCompat = "1.1.0"
}

object Dependency {
    const val appCompat = "androidx.appcompat:appcompat:${Version.appCompat}"
    const val kotlin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${PluginVersion.kotlin}"
}