object PluginVersion {
    const val gradle = "3.5.3"
    const val kotlin = "1.3.71"
}

object Plugin {
    const val APPLICATION = "com.android.application"
    const val ANDROID_LIBRARY = "com.android.library"
    const val KOTLIN_ANDROID = "kotlin-android"
    const val KOTLIN_EXTENSIONS = "kotlin-android-extensions"
    const val KOTLIN_KAPT = "org.jetbrains.kotlin.kapt"
}

object ClasspathDependency {
    const val GRADLE = "com.android.tools.build:gradle:${PluginVersion.gradle}"
    const val KOTLIN = "org.jetbrains.kotlin:kotlin-gradle-plugin:${PluginVersion.kotlin}"
}

