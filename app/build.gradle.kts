import org.jetbrains.kotlin.gradle.dsl.KotlinJvmOptions

plugins {
    id(Plugin.APPLICATION)
    id(Plugin.KOTLIN_ANDROID)
    id(Plugin.KOTLIN_EXTENSIONS)
}

android {

    compileSdkVersion(MetaData.compileSdkVersion)

    defaultConfig {
        applicationId = MetaData.applicationId
        minSdkVersion(MetaData.minSdkVersion)
        targetSdkVersion(MetaData.targetSdkVersion)
        buildToolsVersion(MetaData.buildToolsVersion)

        versionCode = MetaData.versionCode
        versionName = MetaData.versionName

        testInstrumentationRunner = MetaData.testInstrumentationRunner
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        val options = this as? KotlinJvmOptions
        options?.jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
}

dependencies {
    implementation(Dependency.kotlin)
    implementation(Dependency.appCompat)
}


